package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

type User struct {
	Nick        string       `json:"Nick"`
	Email       string       `json:"Email"`
	CreatedAt   string       `json:"Created_at"`
	Subscribers []Subscriber `json:"Subscribers"`
}

type Subscriber struct {
	Email     string `json:"Email"`
	CreatedAt string `json:"Created_at"`
}

// Структура для пользователей, которых
// необходимо найти
type outputUser struct {
	ID   int          `json:"id"`
	From string       `json:"from"`
	To   string       `json:"to"`
	Path []Subscriber `json:"path,omitempty"`
}


func main() {
	JSONFile := "users.json"
	CSVFile := "input.csv"

	userSlice, err := readJSON(JSONFile)
	if err != nil {
		log.Fatal("Unable to unmarshall JSON", err)
	}

	//Email'ы пользователей, которых необходимо обработать
	emailsToFinedSlice, err := readCSV(CSVFile)
	if err != nil {
		log.Fatal("Unable to read CSV file", err)
	}

	resultSlice := search(emailsToFinedSlice, userSlice)

	err = wirteJSON(resultSlice)
	if err != nil {
		log.Fatal("Unable to write JSON", err)
	}
}

func search(csvData [][]string, userSlice []User) []outputUser {
	graph := makeGraph(userSlice)
	resultSlice := make([]outputUser, 0, len(csvData))
	counter := 1
	// Для каждого email
	for _, line := range csvData {
		userStart := line[0]
		userTarger := line[1]

		// найти
		for _, user := range userSlice {
			if user.Email == userStart {
				sub := Subscriber{user.Email, user.CreatedAt}
				path := breadFirstSearch(graph, sub, userTarger)
				note := outputUser{ID: counter, From: userStart, To: userTarger, Path: path}
				resultSlice = append(resultSlice, note)
				counter++
			}
		}
	}

	return resultSlice
}

func readJSON(fileName string) ([]User, error) {
	userSlice := []User{}
	file, err := os.Open(fileName)

	if err != nil {
		return nil, fmt.Errorf("unable to open file: %s", err)
	}

	defer file.Close()

	byteArray, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("unable to read file: %s", err)
	}

	err = json.Unmarshal(byteArray, &userSlice)
	if err != nil {
		return nil, fmt.Errorf("unable to unmarshal file: %s", err)
	}

	return userSlice, nil
}

func readCSV(fileName string) ([][]string, error) {
	var result [][]string

	file, err := os.Open(fileName)
	if err != nil {
		return nil, fmt.Errorf("unable to open CSV file: %s", err)
	}

	defer file.Close()

	reader := csv.NewReader(file)

	for {
		linesArray, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}

			return nil, fmt.Errorf("the error occurred while reading CSV file: %s", err)
		}

		result = append(result, linesArray)
	}

	return result, nil
}

func makeGraph(slice []User) map[string][]Subscriber {
	graph := make(map[string][]Subscriber)

	for _, user := range slice {
		for _, subscriber := range user.Subscribers {
			graph[subscriber.Email] = append(graph[subscriber.Email], Subscriber{
				Email:     user.Email,
				CreatedAt: user.CreatedAt,
			})
		}
	}

	return graph
}

func breadFirstSearch(graph map[string][]Subscriber, start Subscriber, target string) []Subscriber {
	if target == start.Email {
		return nil
	}

	visited := []Subscriber{}

	stack := [][]Subscriber{{start}}

	for len(stack) > 0 {
		path := stack[0]
		stack = stack[1:]

		node := path[len(path)-1]

		if !isVisited(visited, node) {
			users := graph[node.Email]

			for _, user := range users {
				fullPath := make([]Subscriber, len(path))
				copy(fullPath, path)
				fullPath = append(fullPath, user)
				stack = append(stack, fullPath)

				if user.Email == target {
					return fullPath[1 : len(fullPath)-1]
				}
			}

			visited = append(visited, node)
		}
	}

	return nil
}

func isVisited(s []Subscriber, node Subscriber) bool {
	for i := 0; i < len(s); i++ {
		if s[i].Email == node.Email {
			return true
		}
	}

	return false
}

func wirteJSON(jsonSlice []outputUser) error {
	file, err := os.Create("result.json")
	if err != nil {
		return fmt.Errorf("unable to write result file: %s", err)
	}
	defer file.Close()

	res := json.NewEncoder(file)
	res.SetIndent("   ", "\t")

	err = res.Encode(jsonSlice)
	if err != nil {
		return fmt.Errorf("unable to write encode result  file: %s", err)
	}

	return nil
}


